package hino.dto;

import java.util.List;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class HallOfFame {

  private List<Champion> champions;
}
