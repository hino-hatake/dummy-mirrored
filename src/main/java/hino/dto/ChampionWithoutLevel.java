package hino.dto;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class ChampionWithoutLevel {

  private String name;
}
