package hino;

import hino.dto.Champion;
import hino.dto.ChampionWithoutLevel;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.modelmapper.ModelMapper;

/**
 * Just to take a note that we can assert object equality because we use {@link Data @Data} lombok on them.
 */
@Slf4j
public class ModelMapperTest {

  private static final ModelMapper modelMapper = new ModelMapper();

  @Tag("lol")
  @Test
  void override() {

    Champion src = Champion.builder().name("yasuo").build();
    Champion dst = Champion.builder().name("zed").level(18).build();

    log.info("target before mapped: {}:", dst);
    modelMapper.map(src, dst);
    log.info("target after mapped: {}:", dst);

    // we should see that the src is mapped - transformed - mirrored into the dst
    Assertions.assertEquals(src, dst);
  }

  @Test
  void fillNotOverride() {

    // prepare src without level
    ChampionWithoutLevel src = ChampionWithoutLevel.builder().name("yasuo").build();
    // prepare dst with level
    Champion dst = Champion.builder().name("zed").level(18).build();
    Integer originLevel = dst.getLevel();

    log.info("target before mapped: {}:", dst);
    modelMapper.map(src, dst);
    log.info("target after mapped: {}:", dst);

    // let's see if the level remains the same, is not overridden into null
    Assertions.assertAll("You dumb ass, re-check the ModelMapper docs!",
        // assert that name is mapped
        () -> Assertions.assertEquals(src.getName(), dst.getName()),
        // assert that the level remains the same
        () -> Assertions.assertEquals(originLevel, dst.getLevel()));
  }
}
